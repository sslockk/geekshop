from django.contrib import admin
from .models import ProductCategory, Product, Picture, Manufacturer, Contact, PriceProduct


class PictureInline(admin.TabularInline):
	model = Picture
	fk_name = 'related_obj'
	fields=('title', 'active', 'image' )


class ProductCategoryAdmin(admin.ModelAdmin):
	list_display = ('title', 'active', 'sort')
	inlines = (PictureInline,)

admin.site.register(ProductCategory,ProductCategoryAdmin)


class PictureAdmin(admin.ModelAdmin):
	list_display = ('title', 'active', 'sort')
	fields=('title', 'active', 'image', 'sort', 'description' )

admin.site.register(Picture, PictureAdmin)

class PriceInline(admin.TabularInline):
	model = PriceProduct
	fk_name = 'prod_price'
	fields=('price', )

class ProductAdmin(admin.ModelAdmin):
	list_display = ('title', 'active', 'sort')
	inlines = (PriceInline, PictureInline)
	exclude=('pictures', )

admin.site.register(Product,ProductAdmin)

admin.site.register(Manufacturer)
admin.site.register(Contact)
admin.site.register(PriceProduct)