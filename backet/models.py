from django.db import models
from django.utils.translation import ugettext_lazy as _

# warenkorb
# Create your models here.
class Core(models.Model):
    title = models.CharField(_('dc-title'), max_length=250, default='', blank=True, null=False)
    description = models.TextField(_('dc-description'), null=True, blank=True)
    sort = models.IntegerField(_(u'sort'), default=0, blank=True, null=True)
    active = models.BooleanField(_(u'active'), default=False)

    def __str__(self):
        return f'{self.title}'

class Picture(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Картинка')
        verbose_name_plural = _('Картинки')

    image=models.ImageField(upload_to='images')
    related_obj=models.ForeignKey(Core, verbose_name=_(u'pictures'), null=True, blank=True, related_name='pictures', on_delete=models.SET_NULL)


class ProductCategory(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Продуктовая категория')
        verbose_name_plural = _('Продуктовые категории')


class Product(Core):
    """docstring for ProductCategory"""
    class Meta:
        verbose_name = _('Продукт')
        verbose_name_plural = _('Продукты')

    category = models.ForeignKey(ProductCategory, null=True, on_delete=models.SET_NULL)
    prod = models.ForeignKey('Manufacturer', null=True, on_delete=models.SET_NULL)


class Manufacturer(models.Model):
   def __str__(self):
      return f'{self.title}'
   class Meta:
      verbose_name = _('Производитель')
      verbose_name_plural = _('Производитель')

   title = models.CharField(_('dc-manufacturer'), max_length=250, default='', blank=True, null=False)



class Contact(models.Model):
   adress = models.TextField(_('dc-adress'), null=True, blank=True)
   phone = models.CharField(_('dc-phone'), max_length=250, default='', blank=True, null=True)
   emaile = models.CharField(_('dc-emaile'), max_length=250, default='', blank=True, null=True)

   class Meta:
      verbose_name = _('Контакты')
      verbose_name_plural = _('Контакты')

   owner = models.ForeignKey(Manufacturer, null=True, on_delete=models.SET_NULL)


class PriceProduct(models.Model):
   def __str__(self):
      return f'{self.price}'
   price = models.DecimalField(_('dc-price'), null=True, blank=True, decimal_places=2, max_digits=10)

   class Meta:
      verbose_name = _('Цена')
      verbose_name_plural = _('Цена')

   prod_price = models.ForeignKey(Product, null=True, on_delete=models.SET_NULL, related_name='prices')